// Variablendeklarationen
var ausdruck = "";
var check = 0;
var schritt = 0;
var operatoren = ['+', '-', '*', '/'];
var operatorenFalsch = ['+', '-', '*', '/', '', '', '', '', '', '', '', '', '', ''];
var zahlen = [1, 2, 3, 4, 5, 6, 7, 8, 9];
var q2 = [2, 3];
//var next = 1;
var state = 1;
var anzKAuf = 0;
var anzKZu = 0;
var randZ;
var randO;
var randStateQ2;
var anzZahlen = 0;
var keller = "";

// GUI-Generierung (Buttons, Texte, Textbox)
function setup() {
  createCanvas(1500, 1000);
  labelLaenge = createElement('h3', 'Länge des zufälligen Ausdrucks:');
  labelLaenge.position(30, 0);

  input = createInput();
  input.position(labelLaenge.x + 270, labelLaenge.y + 20);

  //labelCheck = createElement('h3', 'Eigener Ausdruck:');
  //labelCheck.position(540, 0);

  //inputCheck = createInput();
  //inputCheck.position(labelCheck.x + 160, labelLaenge.y + 20);

  buttonSubmit = createButton('Generieren');
  buttonSubmit.position(labelLaenge.x, 50);
  buttonSubmit.mousePressed(ausdruckGenerieren);
  
  buttonSubmit2 = createButton('Falsch Generieren');
  buttonSubmit2.position(labelLaenge.x, buttonSubmit.height + 50);
  buttonSubmit2.mousePressed(ausdruckFalschGenerieren);
  
  buttonNextStart = createButton('Erster Schritt');
  buttonNextStart.position(labelLaenge.x + buttonSubmit.width, 50);
  buttonNextStart.mousePressed(nextStart);
  
  buttonNext = createButton('Nächster Schritt');
  buttonNext.position(labelLaenge.x + buttonSubmit.width + buttonNextStart.width, 50);
  buttonNext.mousePressed(next);

  textSize(10);
  text("Anleitung:", input.x + input.width + 370, 17);
  text("Generieren: Generiert einen zufälligen richtigen Ausdruck", input.x + input.width + 370, 34);
  text("Falsch Generieren: Generiert einen zufälligen falschen Ausdruck", input.x + input.width + 370, 51);
  text("Erster Schritt: Startzustand für die schrittweise Generierung eines Ausdrucks", input.x + input.width + 370, 68);
  text("Nächster Schritt: Nächster Zustand des schrittweise generierten Ausdrucks", input.x + input.width + 370, 85);
  textSize(12);
  text("KA = (Q, ∑, Γ, δ, q0, F)", input.x + input.width + 370, 120);
  text("Q = {q1, q2, q3, q4}", input.x + input.width + 370, 140);
  text("Σ = {(, ), Z, O}", input.x + input.width + 370, 160);
  text("Z = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9}", input.x + input.width + 370, 180);
  text("O = {+, -, /, *}", input.x + input.width + 370, 200);
  text("Γ = {$, (}", input.x + input.width + 560, 140);
  text("δ: Überführungsfunktion", input.x + input.width + 560, 160);
  text("q0: q1", input.x + input.width + 560, 180);
  text("F: q4", input.x + input.width + 560, 200);
  
  //buttonCheck = createButton('Prüfen');
  //buttonCheck.position(labelCheck.x, 50);
  //buttonCheck.mousePressed(next);

  stroke(255);
  textSize(20);
  fill(255, 255, 255);

  fill(0, 0, 0);
  textSize(12);
  /*text("Anleitung:", input.x + input.width + 50, 17);
  text("Generieren: Generiert einen zufälligen Ausdruck", input.x + input.width + 50, 37);
  text("Erster Schritt: Startzustand für die schrittweise Generierung eines Ausdrucks", input.x + input.width + 50, 57);
  text("Nächster Schritt: Nächster Zustand des schrittweise generierten Ausdrucks", input.x + input.width + 50, 77);
  */textSize(20);
  text("A", 30, 120);
  markState(".", ".");
  markState_automat(state);
}

function draw() {
  if(check == 0){
    stroke(0);
    textSize(10);
    fill(255, 255, 255);

    // Zustandsübergänge
    fill(0, 0, 0);
    line(430, 150, 485, 150);   // Anfangszustand --> 1
    line(515, 150, 585, 150);   // 1 --> 2
    line(715, 150, 785, 150);   // 1 --> 3
    fill(255, 255, 255);
    arc(650, 135, 100, 80, 2 * PI / 2, 4 * PI / 2);   // 3 --> 1
    arc(650, 165, 100, 80, 4 * PI / 2, 2 * PI / 2);   // 3 --> 1
    arc(590, 135, 10, 60, 1.5 * PI / 2, 0.3 * PI / 2);   // 3 --> 1
    arc(710, 135, 10, 60, 2 * PI / 2, 0.7 * PI / 2);   // 3 --> 1
    
    fill(0, 0, 0);
    stroke(255);
    text("ε, ε → $", 530, 140);
    text("(, ε → (", 550, 100);
    text("O, ε → ε", 630, 90);
    text("Z, ε → ε", 630, 220);
    text("), ( → ε", 720, 100);
    text("ε, $ → ε", 730, 140);

    stroke(0);
    fill(0, 0, 0);
    // Pfeile
    push();
    var angle = atan2(200 - 200, 10 - 75); //gets the angle of the line
    translate(482, 150); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.4, 15, 15*0.4, 15, 0, -4/2); 
    pop();

    push();
    var angle = atan2(-90, 20); //gets the angle of the line
    translate(600, 133); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.4, 15, 15*0.4, 15, 0, -4/2); 
    pop();

    push();
    var angle = atan2(90, -20); //gets the angle of the line
    translate(700, 167); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.4, 15, 15*0.4, 15, 0, -4/2); 
    pop();

    push();
    var angle = atan2(200 - 200, 10 - 75); //gets the angle of the line
    translate(582, 150); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.4, 15, 15*0.4, 15, 0, -4/2); 
    pop();

    push();
    var angle = atan2(200 - 200, 10 - 75); //gets the angle of the line
    translate(782, 150); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.4, 15, 15*0.4, 15, 0, -4/2); 
    pop();

    push();
    var angle = atan2(-90, -10); //gets the angle of the line
    translate(585, 139); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.4, 15, 15*0.4, 15, 0, -4/2); 
    pop();

    push();
    var angle = atan2(-90, 10); //gets the angle of the line
    translate(705, 134); //translates to the destination vertex
    rotate(angle-HALF_PI); //rotates the arrow point
    triangle(-15*0.4, 15, 15*0.4, 15, 0, -4/2); 
    pop();

    checkValidity(input.value());
  }
  check = 1;
}

// Falls auf den Button "Generieren" geklickt wird,
// wird diese Funktion ausgeführt.
// Der Ausdruck wird durch eine zufällige Auswahl aus
// den beiden Arrays für die Operanden und Zahlen ganz 
// oben generiert, indem die Schleife in der Funktion
// so oft, wie die eingegebene Zahl in der Textbox, durchläuft.
async function ausdruckGenerieren(){
  /*textSize(20);
  stroke(255);
  if(input.value() <= 0){
    return;
  }
  ausdruck = "";
  
  var randZ = Math.floor(Math.random()*zahlen.length);
  var randO = Math.floor(Math.random()*operatoren.length);
  var valZ = zahlen[randZ];
  var valO = operatoren[randO];
  
  ausdruck = ausdruck + valZ;
  fill(255, 255, 255);
  rect(20, 250, 1000, 1000);
  fill(0, 0, 0);
  text(ausdruck, 30, 280);
  markState("", valZ);
*/
  ausdruck = "";
  nextStart();
  var laenge = input.value();
  while((anzZahlen < laenge) || (anzKZu < anzKAuf)){
    await sleep(200);
    /*randZ = Math.floor(Math.random()*zahlen.length);
    randO = Math.floor(Math.random()*operatoren.length);
    valZ = zahlen[randZ];
    valO = operatoren[randO];

    if(ausdruck.includes("\n-> ")){
      ausdruck = ausdruck + "\n-> " + "(" + split(ausdruck, "\n-> ")[i-2] + valO + valZ + ")";
    } else{
      ausdruck = ausdruck + "\n-> " + "(" + ausdruck + valO + valZ + ")";
    }*/
    next();
    /*fill(255, 255, 255);
    rect(20, 250, 1000, 1000);
    fill(0, 0, 0);
    text(ausdruck, 30, 280);
    markState(valO, valZ);*/
  }
}

async function ausdruckFalschGenerieren(){
  ausdruck = "";
  nextStart();
  stroke(0);
  fill(255, 255, 255);
  ellipse(500, 150, 30, 30);  // Zustand 1
  ellipse(500, 150, 27, 27);  // Zustand 1
  ellipse(600, 150, 30, 30);  // Zustand 2
  ellipse(700, 150, 30, 30);  // Zustand 3
  ellipse(800, 150, 30, 30);  // Zustand 3
  ellipse(800, 150, 27, 27);  // Zustand 1
  stroke(255);
  fill(0, 0, 0);
  textSize(12);
  text("q1", 494, 153);
  text("q2", 594, 153);
  text("q3", 694, 153);
  text("q4", 794, 153);
  var laenge = input.value();
  while((anzZahlen < laenge) || (anzKZu < anzKAuf)){
    nextFalsch();
  }
}

// Wird ausgeführt, wenn auf "Erster Schritt" geklickt wird.
// Dabei werden alle benötigten Startwerte für die schrittweise
// Generierung eines Ausdrucks gesetzt, damit mit dem Button
// "Nächster Schritt" die nächsten Schritte korrekt ausgeführt werden.
function nextStart(){
  anzKAuf = 0;
  anzKZu = 0;
  anzZahlen = 0;
  ausdruck = "";
  textSize(20);
  if(input.value() <= 0){
    return;
  }
  state = 1;
  schritt = 1;
  fill(255, 255, 255);
  next();
}

// Wird ausgeführt, wenn auf "Nächster Schritt" geklickt wird.
// Mithilfe der Variablen, sowie Arrays ganz oben, wird der 
// nächste, zufällige Schritt generiert.
function next(){
  var laenge = input.value();
  var valZ;
  var valO;
  if(anzZahlen < laenge){
    if(state == 1){
      keller = "Keller\n$ ";
      state = 2;
    }
    else if(state == 2){
      state = q2[Math.floor(Math.random()*q2.length)];
      if(state == 3 && anzKAuf > 0){
        randZ = Math.floor(Math.random()*zahlen.length);
        valZ = zahlen[randZ];
        ausdruck = ausdruck + valZ;
        anzZahlen++;
      }
      else{
        state = 2;
        ausdruck = ausdruck + "(";
        anzKAuf++;
        keller = keller + "( ";
      }
    } 
    else if(state == 3){
      state = q2[Math.floor(Math.random()*q2.length)];
      if(state == 3){
        if(anzKZu < anzKAuf){
          ausdruck = ausdruck + ")";
          anzKZu++;
          keller = keller.replace("( ", '');
        }
      } else{
        randO = Math.floor(Math.random()*operatoren.length);
        valO = operatoren[randO];
        ausdruck = ausdruck + valO;
      }
    } 
  }
  else{
    if(anzKAuf >= anzKZu && state != 4){
      ausdruck = ausdruck + ")";
      anzKZu++;
      keller = keller.replace("( ", '');
    }
  }
  if(anzKZu == anzKAuf && anzZahlen == laenge){
    state = 4;
    keller = "Keller\n";
  }
  stroke(255);
  fill(255, 255, 255);
  rect(20, 250, 1000, 1000);
  rect(450, 20, 300, 50);
  fill(0, 0, 0);
  textSize(20);
  text(ausdruck, 30, 280);
  text(keller, 500, 20);
  markState(valO, valZ);
  markState_automat(state);
}

// Parameter
// operand: Der aktuell ausgewählte Operand
// zahl: Die aktuell ausgewählte Zahl
// 
// Färbt den aktuellen Operanden und die aktuelle Zahl grün.
function markState(operand, zahl){
  stroke(255);
  fill(255, 255, 255);
  rect(25, 130, 400, 30);
  rect(25, 160, 400, 30);
  textSize(20);
  fill(0, 0, 0);

  text("O → ", 28, 150);
  if(operand == '+'){
    fill(0, 200, 0);
    text("+", 80, 150);
    fill(0, 0, 0);
  } else text("+", 80, 150);
  text("|", 96, 150);
  if(operand == '-'){
    fill(0, 200, 0);
    text("-", 110, 150);
    fill(0, 0, 0);
  } else text("-", 110, 150);
  text("|", 126, 150);
  if(operand == '*'){
    fill(0, 200, 0);
    text("*", 140, 150);
    fill(0, 0, 0);
  } else text("*", 140, 150);
  text("|", 156, 150);
  if(operand == '/'){
    fill(0, 200, 0);
    text("/", 170, 150);
    fill(0, 0, 0);
  } else text("/", 170, 150);

  text("Z → ", 31, 180);
  if(zahl == 0){
    fill(0, 200, 0);
    text("0", 80, 180);
    fill(0, 0, 0);
  } else text("0", 80, 180);
  text("|", 97, 180);
  if(zahl == 1){
    fill(0, 200, 0);
    text("1", 110, 180);
    fill(0, 0, 0);
  } else text("1", 110, 180);
  text("|", 127, 180);
  if(zahl == 2){
    fill(0, 200, 0);
    text("2", 140, 180);
    fill(0, 0, 0);
  } else text("2", 140, 180);
  text("|", 157, 180);
  if(zahl == 3){
    fill(0, 200, 0);
    text("3", 170, 180);
    fill(0, 0, 0);
  } else text("3", 170, 180);
  text("|", 187, 180);
  if(zahl == 4){
    fill(0, 200, 0);
    text("4", 200, 180);
    fill(0, 0, 0);
  } else text("4", 200, 180);
  text("|", 217, 180);
  if(zahl == 5){
    fill(0, 200, 0);
    text("5", 230, 180);
    fill(0, 0, 0);
  } else text("5", 230, 180);
  text("|", 247, 180);
  if(zahl == 6){
    fill(0, 200, 0);
    text("6", 260, 180);
    fill(0, 0, 0);
  } else text("6", 260, 180);
  text("|", 277, 180);
  if(zahl == 7){
    fill(0, 200, 0);
    text("7", 290, 180);
    fill(0, 0, 0);
  } else text("7", 290, 180);
  text("|", 307, 180);
  if(zahl == 8){
    fill(0, 200, 0);
    text("8", 320, 180);
    fill(0, 0, 0);
  } else text("8", 320, 180);
  text("|", 337, 180);
  if(zahl == 9){
    fill(0, 200, 0);
    text("9", 350, 180);
    fill(0, 0, 0);
  } else text("9", 350, 180);
  if(operand == '$' || zahl == '?'){
    markState_automat(3);
  }
  stroke(255);
}

function markState_automat(zustand){
  stroke(0);
  fill(255, 255, 255);
  ellipse(500, 150, 30, 30);  // Zustand 1
  ellipse(500, 150, 27, 27);  // Zustand 1
  ellipse(600, 150, 30, 30);  // Zustand 2
  ellipse(700, 150, 30, 30);  // Zustand 3
  ellipse(800, 150, 30, 30);  // Zustand 3
  ellipse(800, 150, 27, 27);  // Zustand 1
  stroke(255);
  fill(0, 0, 0);
  textSize(12);
  text("q1", 494, 153);
  text("q2", 594, 153);
  text("q3", 694, 153);
  text("q4", 794, 153);
  if(zustand == 1){
    stroke(0);
    fill(0, 255, 0);
    ellipse(500, 150, 27, 27);  // Zustand 1
    stroke(255);
    fill(0, 0, 0);
    textSize(12);
    text("q1", 494, 153);
  } else if(zustand == 2){
    stroke(0);
    fill(0, 255, 0);
    ellipse(600, 150, 30, 30);  // Zustand 1
    stroke(255);
    fill(0, 0, 0);
    textSize(12);
    text("q2", 594, 153);
  } else if(zustand == 3){
    stroke(0);
    fill(0, 255, 0);
    ellipse(700, 150, 30, 30);  // Zustand 2
    stroke(255);
    fill(0, 0, 0);
    textSize(12);
    text("q3", 694, 153);
  } else if(zustand == 4){
    stroke(0);
    fill(0, 255, 0);
    ellipse(800, 150, 27, 27);  // Zustand 4
    stroke(255);
    fill(0, 0, 0);
    textSize(12);
    text("q4", 794, 153);
  }
}

// Hilfsfunktion, um eine bestimmte Zeit die Ausführung
// einer Funktion anzuhalten.
function sleep(millisecondsDuration)
{
  return new Promise((resolve) => {
    setTimeout(resolve, millisecondsDuration);
  })
}

function nextFalsch(){
  var laenge = input.value();
  var valZ;
  var valO;
  if(anzZahlen < laenge){
    if(state == 1){
      state = 2;
    }
    else if(state == 2){
      state = q2[Math.floor(Math.random()*q2.length)];
      if(state == 3 && anzKAuf > 0){
        randZ = Math.floor(Math.random()*zahlen.length);
        valZ = zahlen[randZ];
        ausdruck = ausdruck + valZ;
        anzZahlen++;
      }
      else{
        state = 2;
        ausdruck = ausdruck + "(";
        anzKAuf++;
      }
    } 
    else if(state == 3){
      state = q2[Math.floor(Math.random()*q2.length)];
      if(state == 3){
        if(anzKZu < anzKAuf){
          ausdruck = ausdruck + ")";
          anzKZu++;
        }
      } else{
        randO = Math.floor(Math.random()*operatorenFalsch.length);
        valO = operatorenFalsch[randO];
        ausdruck = ausdruck + valO;
      }
    } 
  }
  else{
    if(anzKAuf >= anzKZu && state != 4){
      ausdruck = ausdruck + ")";
      anzKZu++;
    }
  }
  if(anzKZu == anzKAuf && anzZahlen == laenge){
    state = 4;
  }
  stroke(255);
  fill(255, 255, 255);
  rect(20, 250, 1000, 1000);
  rect(500, 20, 300, 50);
  fill(0, 0, 0);
  textSize(20);
  text(ausdruck, 30, 280);
}